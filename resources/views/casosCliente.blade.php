@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
            <div style="text-align: center">
       
        <a href="{{route('casos.create',$cliente->id)}}" class="btn btn-primary">Crear Caso</a>
    </div>
            <div class="card">
                <div class="card-header">Casos Cliente: {{$cliente->Nombre }}</div>

                <div class="container">
  <table class="table">
    <thead>
      <tr>
        <th>NUMERO DE CASO</th>
        <th>FOLIO</th>
        
        <th>NOMBRE</th>
        <th>REQUERIMIENTOS</th>
        <th>COMENTARIO</th>
        <th>STATUS</th>
        
      </tr>
    </thead>
    <tbody>
    @if(isset($clientes))
    @foreach ($casos as $caso)

      <tr>
      <td>{{$caso->Folio }}</td>
      <td>{{$caso->Nombre }}</td>
     
      <td>{{$caso->Requerimiento }}</td>
      <td>{{$caso->Comentario}}</td>
       <td>{{$caso->Status }}</td>
      <td><a href="{{ route('casos.edit',$caso->id) }}">Editar</a>
          <a onclick="return confirm('¿Esta seguro que desea Eliminar este registro?')" href="{{ route('casos.delete',$cliente->id) }}" >Eliminar</a>
         </td>
       
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>
  @if(isset($clientes))
  {{ $clientes->render()}}
  @endif
</div>
            </div>
        </div>
    </div>
</div>
@endsection
