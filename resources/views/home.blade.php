@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
            <div style="text-align: center">
        
        <a href="{{route('home.create')}}" class="btn btn-primary">Crear Cliente</a>
    </div>
            <div class="card">
                <div class="card-header">Clientes</div>

                <div class="container">
  <table class="table">
    <thead>
      <tr>
        <th>NOMBRE</th>
        <th>APELLIDO</th>
        
        <th>DUI</th>
        <th>NIT</th>
        <th>DIRECCION</th>
        <th>TELEFONO</th>
        <th>CELULAR</th>
        <th>NOTAS</th>
        <th>STATUS</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($clientes as $cliente)

      <tr>
      <td>{{$cliente->Nombre }}</td>
      <td>{{$cliente->Apellido }}</td>
      <td>{{$cliente->Dui }}</td>
      <td>{{$cliente->Nit }}</td>
      <td>{{$cliente->Direccion }}</td>
      <td>{{$cliente->Telefono }}</td>
      <td>{{$cliente->Celular }}</td>
      <td>{{$cliente->Notas }}</td>
      <td>{{$cliente->Status }}</td>
      <td><a href="{{ route('home.edit',$cliente->id) }}">Editar</a>
          <a onclick="return confirm('¿Esta seguro que desea Eliminar este registro?')" href="{{ route('home.delete',$cliente->id) }}" >Eliminar</a>
          <a href="{{ route('home.casos',$cliente->id) }}">Casos</a></td>
       
      </tr>
      @endforeach
    </tbody>
  </table>
 {{ $clientes->render()}}
</div>
            </div>
        </div>
    </div>
</div>
@endsection
