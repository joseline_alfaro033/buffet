@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
       
<div class="">
<form method="POST"  action="{{route('casos.store')}}">
<input type="hidden" name="id_cliente" value="{{$cliente->id}}">
            @csrf
            <div class="form-group">
                  <label class="mt-4" for="apellido">Folio:</label>
                  <input type="text" class="form-control" id="apellido" name="Folio" autocomplete="off" required placeholder="Escribe folio del caso">
                </div>
                <div class="form-group">
                  <label class="mt-4" for="nombre">Nombre:</label>
                  <input type="text" class="form-control" id="nombre" name="Nombre" autocomplete="off" required placeholder="Escribe nombre del caso">
                </div>
                
                <div class="form-group">
                  <label class="mt-4" for="nombre">Requerimientos:</label>
                  <input type="text" class="form-control" id="nombre" name="Requerimientos" autocomplete="off" required placeholder="Escribe Requerimientos del caso">
                </div>
                <div class="form-group">
                  <label class="mt-4" for="nombre">Comentarios:</label>
                  <input type="text" class="form-control" id="nombre" name="Comentarios" autocomplete="off" required placeholder="Escribe Comentarios del caso">
                </div>
                
                <div class="form-group">
                  <label class="mt-2" for="descripcion">Status:</label>
                  <select  class="form-control" name="Status" id="Status">
											<option selected>Seleccione</option>
											<option value="Activo">Activo</option>
											<option value="Inactivo">Inactivo</option>
											
										</select>
                   </div>
                <button type="submit" class="btn btn-outline-success mt-3 float-right">Guardar</button>
              </form>
             
              </div>
              </div>
              </div>
              </div>
@endsection