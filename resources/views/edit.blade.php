@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
       
<div class="">
<form method="POST"  action="{{route('home.update')}}">
            @csrf
            <input type="hidden" name="id" value="{{$cliente->id}}">
                <div class="form-group">
                  <label class="mt-4" for="nombre">Nombre:</label>
                  <input type="text" class="form-control" id="nombre"  value="{{$cliente->Nombre}}" name="Nombre" autocomplete="off" required placeholder="Escribe nombre del cliente">
                </div>
                <div class="form-group">
                  <label class="mt-4" for="apellido">Apellido:</label>
                  <input type="text" class="form-control" id="apellido"  value="{{$cliente->Apellido}}" name="Apellido" autocomplete="off" required placeholder="Escribe apellido del cliente">
                </div>
                <div class="form-group">
                  <label class="mt-4" for="nombre">Dui:</label>
                  <input type="text" class="form-control" id="nombre"  value="{{$cliente->Dui}}" name="Dui" autocomplete="off" required placeholder="Escribe dui del cliente">
                </div>
                <div class="form-group">
                  <label class="mt-4" for="nombre">Nit:</label>
                  <input type="text" class="form-control" id="nombre" name="Nit"  value="{{$cliente->Nit}}" autocomplete="off" required placeholder="Escribe nit del cliente">
                </div>
                <div class="form-group">
                  <label class="mt-4" for="nombre">Direccion:</label>
                  <input type="text" class="form-control" id="nombre" name="Direccion"  value="{{$cliente->Direccion}}" autocomplete="off" required placeholder="Escribe direccion del cliente">
                </div>
                <div class="form-group">
                  <label class="mt-2" for="descripcion">Telefono:</label>
                  <input type="text" class="form-control" id="descripcion" name="Telefono" value="{{$cliente->Telefono}}" autocomplete="off" required placeholder="Escribe telefono del cliente">
                </div>
                <div class="form-group">
                  <label class="mt-2" for="descripcion">Celular:</label>
                  <input type="text" class="form-control" id="descripcion" name="Celular" value="{{$cliente->Celular}}"  autocomplete="off" required placeholder="Escribe celular del cliente">
                </div>
                <div class="form-group">
                  <label class="mt-2" for="descripcion">Notas:</label>
                  <input type="text" class="form-control" id="descripcion" name="Notas"  value="{{$cliente->Notas}}" autocomplete="off" required placeholder="Escriba notas">
                </div>
                <div class="form-group">
                  <label class="mt-2" for="descripcion">Status:</label>
                  <select  class="form-control" name="Status" id="Status">
											<option selected>Seleccione</option>
											<option value="Activo">Activo</option>
											<option value="Inactivo">Inactivo</option>
											
										</select>
                   </div>
                <button type="submit" class="btn btn-outline-success mt-3 float-right">Guardar</button>
              </form>
             
              </div>
              </div>
              </div>
              </div>
@endsection