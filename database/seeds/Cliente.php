<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class Cliente extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 15; $i++) {
              \DB::table("clientes")->insert(
                    array(
                         
                          'nombre'     => $faker->name,
                          'Apellido' => $faker->lastname,
                          'Dui' => rand(1, 99),
                          'Nit' => rand(1, 99),
                          'Direccion' => $faker->address,
                          'Telefono' => $faker->PhoneNumber,
                          'Celular' => $faker->PhoneNumber,
                          'Notas' => $faker->text,
                          'Status' => 'Activo',
                          'created_at' => date('Y-m-d H:m:s'),
                          'updated_at' => date('Y-m-d H:m:s')
                    )
              );
        }
        
    }
}
