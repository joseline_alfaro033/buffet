<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class Caso extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 15; $i++) {
          DB::table('casos')->insert([
         	            
              'Folio' => rand(1, 99),
              'Nombre' => $faker->sentence,
              'Requerimientos' => $faker->sentence,
              'Comentarios' => $faker->text,
              
              'Status' => 'Activo',
              'id_cliente'=> $this->getRandomClienteId(),
          ]);
        }
    }
    private function getRandomClienteId() {
        $cli = \App\Cliente::inRandomOrder()->first();
        return $cli->id;
    }
}
