<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table='clientes';
    protected $primaryKey = 'id';
    protected $fillable = array('Nombre','Apellido');
    protected $hidden = ['created_at','updated_at']; 

    public function casos()
	{	
		return $this->hasMany('App\Caso', 'id_cliente','id');
	}
}
