<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Caso;
use App\Cliente;

class CasoController extends Controller
{
    public function index($id)
    {
        $casos = Caso::where('id_cliente',$id)->paginate(10);
    return view('casosCliente',['casos'=>$casos]);
       
    }
    public function create( $id){
        $cliente = Cliente::findOrFail($id);
        return view('createCaso',['cliente'=>$cliente]);
    }
    
    public function store(Request $request){
    
        $caso = new Caso;
    
        $caso->Folio= $request->get('Folio');
        $caso->Nombre= $request->get('Nombre');
        $caso->Requerimientos= $request->get('Requerimientos');
        $caso->Comentarios= $request->get('Comentarios');
       
        $caso->status= $request->get('Status');
        $caso->id_cliente=$request->get('id_cliente');
    
        $caso->save();
    
        $casos = Caso::paginate(10);
        return redirect('home')->with('success-message', '¡Datos actualizados exitosamente!');
    
    }
    
    public function edit($id){
    
        $caso = Caso::findOrFail($id);
    
        return view('edit',compact('caso'));
    }
    
    public function update(Request $request){
    
        $caso = Caso::findOrFail($request->get('id'));
        $caso->Folio= $request->get('Folio');
        $caso->Nombre= $request->get('Nombre');
        $caso->Requerimientos= $request->get('Requerimientos');
        $caso->Comentarios= $request->get('Comentarios');
       
        $caso->status= $request->get('Status');
        $caso->update();
    
        return redirect('casosCliente')->with('success-message', '¡Datos actualizados exitosamente!');
    }
    
    public function delete($id){
    
        $caso = Caso::findOrFail($id);
    
        $caso->delete();
    
        $casos = Caso::paginate(10);
        return redirect('casosCliente')->with('success-message', '¡Datos eliminados exitosamente!');
     
    }
}
