<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Caso;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $clientes = Cliente::paginate(10);
    return view('home',['clientes'=>$clientes]);
       
    }
    public function create(Request $request){

        return view('create');
    }
    
    public function casos($id){
        $cliente = Cliente::findOrFail($id);
        $casos = Caso::where('id_cliente',$id);
        return view('casosCliente',['cliente'=>$cliente,'casos'=>$casos]);
    }

    public function store(Request $request){
    
        $cliente = new Cliente;
    
        $cliente->Nombre= $request->get('Nombre');
        $cliente->Apellido= $request->get('Apellido');
        $cliente->Dui= $request->get('Dui');
        $cliente->Nit= $request->get('Nit');
        $cliente->Direccion= $request->get('Direccion');
        $cliente->Telefono= $request->get('Telefono');
        $cliente->Celular= $request->get('Celular');
        $cliente->Notas= $request->get('Notas');
        $cliente->status= $request->get('Status');
    
        $cliente->save();
    
        $clientes = Cliente::paginate(10);
        return view('home',['clientes'=>$clientes]);
    }
    
    public function edit($id){
    
        $cliente = Cliente::findOrFail($id);
    
        return view('edit',compact('cliente'));
    }
    
    public function update(Request $request){
    
        $cliente = Cliente::findOrFail($request->get('id'));
        $cliente->Nombre= $request->get('Nombre');
        $cliente->Apellido= $request->get('Apellido');
        $cliente->Dui= $request->get('Dui');
        $cliente->Nit= $request->get('Nit');
        $cliente->Direccion= $request->get('Direccion');
        $cliente->Telefono= $request->get('Telefono');
        $cliente->Celular= $request->get('Celular');
        $cliente->Notas= $request->get('Notas');
        $cliente->status= $request->get('Status');
        $cliente->update();
    
        return redirect('home')->with('success-message', '¡Datos actualizados exitosamente!');
    }
    
    public function delete($id){
    
        $cliente = Cliente::findOrFail($id);
    
        $cliente->delete();
    
        $clientes = Cliente::paginate(10);
        return redirect('home')->with('success-message', '¡Datos eliminados exitosamente!');
     
    }



}
