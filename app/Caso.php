<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caso extends Model
{
    protected $table='casos';
    protected $primaryKey = 'id';
    protected $fillable = array('folio');
    protected $hidden = ['created_at','updated_at']; 

    public function cliente()
	{	
		return $this->hasOne('App\Cliente','id','id_cliente');
    }
}
