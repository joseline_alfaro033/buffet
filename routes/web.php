<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home/create','HomeController@create')->name('home.create');

Route::post('/home/store','HomeController@store')->name('home.store');

Route::get('/home/edit/{id}','HomeController@edit')->name('home.edit');

Route::post('/home/update','HomeController@update')->name('home.update');

Route::get('/home/delete/{id}','HomeController@delete')->name('home.delete');

Route::get('/home/casos/{id}','HomeController@casos')->name('home.casos');

Route::get('/casos', 'CasoController@index')->name('casos');

Route::get('/casos/create/{id}','CasoController@create')->name('casos.create');

Route::post('/casos/store','CasoController@store')->name('casos.store');

Route::get('/casos/edit/{id}','CasoController@edit')->name('casos.edit');

Route::post('/casos/update','CasoController@update')->name('casos.update');

Route::get('/casos/delete/{id}','CasoController@delete')->name('casos.delete');